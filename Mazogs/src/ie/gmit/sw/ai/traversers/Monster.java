package ie.gmit.sw.ai.traversers;

import java.awt.Component;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.*;

import ie.gmit.sw.ai.Node;



public class Monster implements Traversator{
	private int energy = 100;
	private int power = 60;
	private Node pos;
	private int beamWidth= 2; 
	
	private Node goal;
	//Get rid of if not neccessary
	 
	
	public Monster(Node target) {
		super();
		
		this.goal = target;
		goal.setTarget(true);
		
	}
	
	public Monster(Node target, Node pos) {
		super();
		
		this.goal = target;
		goal.setTarget(true);
		this.pos = pos;
		
	}
	
	public void move(Node pos1, Node pos2){
		pos = pos1;
		pos1.setFeature('C');
		pos2.setFeature(' ');
		
		System.out.println("Pos: " + pos1.getRow() + " : " + pos1.getCol());
		System.out.println("Pos: " + pos2.getRow() + " : " + pos2.getCol());
		
	}
	
	public void setPos(Node n){
		pos = n;
	}
	
	public Node getPos(){
		return pos;
	}
	
	
	
	

	public void traverse(Node[][] maze, Node node, Component viewer) {
		LinkedList<Node> queue = new LinkedList<Node>();
		queue.addFirst(node);
		
        long time = System.currentTimeMillis();
    	int visitCount = 0;
    	setPos(node);
		while(!queue.isEmpty()){
			
			node = queue.poll();
			if (visitCount == 3){
				
				move(node, pos);
				setPos(node);
				node.setFeature('C');
			}
			node.setVisited(true);	
			visitCount++;
			viewer.repaint();
			
			if (node.isTarget()){
		        time = System.currentTimeMillis() - time; //Stop the clock
		        //TraversatorStats.printStats(node, time, visitCount);
		        viewer.repaint();
				break;
			}
			
			try { //Simulate processing each expanded node
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			Node[] children = node.children(maze);
			Collections.sort(Arrays.asList(children),(Node current, Node next) -> current.getHeuristic(goal) - next.getHeuristic(goal));
			
			int bound = 0;
			if (children.length < beamWidth){
				bound = children.length;
			}else{
				bound = beamWidth;
			}
			
			for (int i = 0; i < bound; i++) {
				if (children[i] != null && !children[i].isVisited()){
					children[i].setParent(node);
					queue.addFirst(children[i]);
				}
			}
		}
	}
	
	
}
