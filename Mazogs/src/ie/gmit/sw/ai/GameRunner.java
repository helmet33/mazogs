package ie.gmit.sw.ai;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;

import javax.swing.*;

import ie.gmit.sw.ai.traversers.BeamTraversator;
import ie.gmit.sw.ai.traversers.BestFirstTraversator;
//import ie.gmit.sw.ai.traversers.DepthLimitedDFSTraversator;
//import ie.gmit.sw.ai.traversers.Monster;
import ie.gmit.sw.ai.traversers.Traversator;
public class GameRunner implements KeyListener{
	private static final int MAZE_DIMENSION = 100;
	private Node[][] model;
	private GameView view;
	private int currentRow;
	private int currentCol;
	private Node goal;
	private Player player;
	//private Monster cat;
	private Node exit;
	
	private Traversator t; 
	//private Traversator catTra;
	
	public GameRunner() throws Exception{
		Maze m = new Maze(MAZE_DIMENSION, MAZE_DIMENSION);
		model = m.getMaze();
    	view = new GameView(model);
    	
    	placePlayer();
    	setGoalNode();
    	
    	
    	Dimension d = new Dimension(GameView.DEFAULT_VIEW_SIZE, GameView.DEFAULT_VIEW_SIZE);
    	view.setPreferredSize(d);
    	view.setMinimumSize(d);
    	view.setMaximumSize(d);
    	
    	JFrame f = new JFrame("GMIT - B.Sc. in Computing (Software Development)");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.addKeyListener(this);
        f.getContentPane().setLayout(new FlowLayout());
        f.add(view);
        f.setSize(1000,1000);
        f.setLocation(100,100);
        f.pack();
        f.setVisible(true);
        
        
	}
	
	public void setGoalNode(){
		
		Random generator = new Random();
		int randRow = generator.nextInt(model.length);
		int randCol = generator.nextInt(model[0].length);
		
		System.out.println( "Goal set to Row: " + randRow + " Col: " + randCol);
		model[randRow][randCol].setGoalNode(true);
		model[randRow][randCol].setFeature(' ');
		goal = model[randRow][randCol];
		
		System.out.println("Exit is set to: " );
		exit = model[1][50];
		exit.setFeature(' ');
	}
	
	private void placePlayer(){   	
    	currentRow = (int) (MAZE_DIMENSION * Math.random());
    	currentCol = (int) (MAZE_DIMENSION * Math.random());
    	model[currentRow][currentCol].setFeature('E');
    	player = new Player(100, model[currentRow][currentCol], model[currentRow][currentCol]);
    	
    	System.out.println( "Player set to Row: " + currentRow + " Col: " + currentCol);
    	updateView(); 		
	}
	
	
	
	private void updateView(){
		view.setCurrentRow(currentRow);
		view.setCurrentCol(currentCol);
		//player.setPos(model[currentRow][currentCol]);
	}

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT && currentCol < MAZE_DIMENSION - 1) {
        	if (isValidMove(currentRow, currentCol + 1)) currentCol++;   		
        }else if (e.getKeyCode() == KeyEvent.VK_LEFT && currentCol > 0) {
        	if (isValidMove(currentRow, currentCol - 1)) currentCol--;	
        }else if (e.getKeyCode() == KeyEvent.VK_UP && currentRow > 0) {
        	if (isValidMove(currentRow - 1, currentCol)) currentRow--;
        }else if (e.getKeyCode() == KeyEvent.VK_DOWN && currentRow < MAZE_DIMENSION - 1) {
        	if (isValidMove(currentRow + 1, currentCol)) currentRow++;        	  	
        }else if (e.getKeyCode() == KeyEvent.VK_Z){
        	view.toggleZoom();
        }else if (e.getKeyCode() == KeyEvent.VK_SPACE){
        	//Test code
        	System.out.println("The Cats are on Holidays");
        	Cater caty = new Cater();
        	caty.start();
        	updateView();
        }else{
        	return;
        }
        
        updateView();       
    }
    public void keyReleased(KeyEvent e) {} //Ignore
	public void keyTyped(KeyEvent e) {} //Ignore

    
	private boolean isValidMove(int r, int c){
		if (r <= model.length -1  && c <= model[r].length -1 && model[r][c].getFeature() == ' ' && !model[r][c].isGoalNode()){
			//model[currentRow][currentCol].setFeature(' ');
			//model[r][c].setFeature('E');
			player.move(model[r][c], model[currentRow][currentCol]);
			//System.out.println(true + "player at: " + player.getPos().getRow() + ":" + player.getPos().getCol());
			return true;
		}else if(r <= model.length -1  && c <= model[r].length -1 && model[r][c].getFeature() == 'B'){
			System.out.println("Picked up bomb");
			model[r][c].setFeature('X');
			return false;
		}else if(r <= model.length -1  && c <= model[r].length -1 && model[r][c].getFeature() == 'W'){
			System.out.println("Picked up Sword");
			model[r][c].setFeature('X');
			return false;
		}else if(r <= model.length -1  && c <= model[r].length -1 && model[r][c].getFeature() == 'H'){
			System.out.println("Push the button. End the World.");
			model[r][c].setFeature('X');
			return false;
		}else if(r <= model.length -1  && c <= model[r].length -1 && model[r][c].getFeature() == '?'){
			Hint hint = new Hint();
			hint.start();
			model[r][c].setFeature(' ');
			updateView();
			return false;
		}else if(r <= model.length -1  && c <= model[r].length -1 && model[r][c].isGoalNode()){
			System.out.println("You have found the dog. The Exit is your new Goal");
			model[r][c].setGoalNode(false);
			exit.setGoalNode(true);
			goal = model[exit.getRow()][exit.getCol()];
			return false;
		}
		else{
			//System.out.println(false);
			return false; //Can't move
		}
	}
	
	public class Hint extends Thread {
		public void run() {
			try {
				Random gen = new Random();
				int choice = gen.nextInt(2);
				//Copy of model for thread to run on
				Node[][] dodel = model.clone();
				//Clear visited tags
				clearCp(dodel);
				//Best first traversal
				// or beam depending
				
				 
				
				if (choice == 1){
					t = new BeamTraversator(goal, 2);
					System.out.println("Beam in operation! Width 2");
				} else {
					t = new BestFirstTraversator(goal);
					System.out.println("BFS in operation Sir!");
				}
				System.out.println();	
				System.out.println("Follow the yellow brick road for 10 seconds.");
				t.traverse(dodel, dodel[currentRow][currentCol], view);
				
				//Sleep for 10 seconds
				Thread.sleep(10000);
				//Clear the view
				clearCp(dodel);
				return;
			} // end try
			catch (Exception e) {
				System.out.println(e.toString());
			}

		}
		
		

		private void clearCp(Node[][] cpMaze) {
			for (int row = 0; row < cpMaze.length; row++) {
				for (int col = 0; col < cpMaze[row].length; col++) {
					cpMaze[row][col].setVisited(false);

				}
			}
		}
	} //end of hint
	
	
	// Cat 
	public class Cater extends Thread {
		// This is just test code the code Attempted is mostly contained in a following method
		public void run() {
			try {
				Node[][] codel = model.clone();
				Node g = codel[3][3];
				Random gen = new Random();
				g.setFeature('C');
				int b = 0;
				while(b < 100){
					int checkMove = gen.nextInt(3);
					if (checkMove == 1 && isValid(g.getRow(), g.getCol() - 1) ){
						g.setFeature(' ');
						g.setCol(g.getCol() + 1);
						g.setFeature('C');
						
					}
					else if (checkMove == 1 && isValid(g.getRow(), g.getCol() - 1) ){
						g.setFeature(' ');
						g.setCol(g.getCol() - 1);
						g.setFeature('C');
					}
					else if (checkMove == 0 && isValid(g.getRow() + 1, g.getCol()) ){
						g.setFeature(' ');
						g.setRow(g.getRow() + 1);
						g.setFeature('C');
					}
					else if (checkMove == 0 && isValid(g.getRow() - 1, g.getCol()) ){
						g.setFeature(' ');
						g.setRow(g.getRow() - 1);
						g.setFeature('C');
					}
					
					Thread.sleep(1000);
					b++;
					view.repaint();
						
				}
			
				
				
				return;
			} // end try
			catch (Exception e) {
				System.out.println(e.toString());
			}
			
			
			
			

		}
		
		private boolean isValid(int r, int c){
			if (r <= model.length -1  && c <= model[r].length -1 && model[r][c].getFeature() == ' '){
				//System.out.println(true);
				
				return true;
			}else {
				//System.out.println(false);
				return false; //Can't move
			}
		}
	}
		
		
//		public class Cater extends Thread {
//			public void run() {
//				try {
//					Monster cat;
//					
//					
//					//Copy of model for thread to run on
//					Node[][] codel = model.clone();
//					Node exit2 = codel[exit.getRow()][exit.getCol()];
//					Node tempMon = codel[5][5] ;
//					//Clear visited tags
//					clearCp(codel);
//					//Best first traversal
//					cat = new Monster(exit2, tempMon);
//					//catTra.setPos(tempMon)
//					//t = new BeamTraversator(goal, 2);
//					Node x = cat.getPos();  
//					
//					System.out.println("Exit before: " + exit2.getCol() + " : " + exit2.getRow());
//					
//					while((x.getRow() != exit2.getRow()) && ((x.getCol() != exit2.getCol()))){
//						
//						tempMon = cat.getPos();
//						cat.traverse(codel, x, view);
//						
//						System.out.println("Hello");
//						
//						x = cat.getPos();
//						clearCp(codel);
//						Thread.sleep(10);
//					}
//					System.out.println("Left the while!");
//					
//					System.out.println(x.getCol() + " : " + x.getRow());
//					System.out.println(exit.getCol() + " : " + exit.getRow());
//					
//					//Sleep for 10 seconds
//					//Thread.sleep(10000);
//					//Clear the view
//					clearCp(codel);
//					return;
//				} // end try
//				catch (Exception e) {
//					System.out.println(e.toString());
//				}
//
//			}		
		

//		private void clearCp(Node[][] cpMaze) {
//			for (int row = 0; row < cpMaze.length; row++) {
//				for (int col = 0; col < cpMaze[row].length; col++) {
//					cpMaze[row][col].setVisited(false);
//
//				}
//			}
//		}
//	} //end of hint
    
    
	
	public static void main(String[] args) throws Exception{
		new GameRunner();
	}
}