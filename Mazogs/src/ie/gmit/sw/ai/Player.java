package ie.gmit.sw.ai;

public class Player {
	private int health = 100;
	//private Weapon = Sword;
	private Node currentPos = null;
	private Node startingPos = null;
	
	
	public Player(int health, Node currentPos, Node startingPos) {
		super();
		this.health = health;
		this.currentPos = currentPos;
		this.startingPos = startingPos;
	}
	
	public void move(Node pos1, Node pos2){
		currentPos = pos1;
		pos1.setFeature('E');
		pos2.setFeature(' ');
		
	}
	
	public Node getPos(){
		return currentPos;
	}
	
	public void setPos(Node pos){
		currentPos = pos;
	}
	

}
